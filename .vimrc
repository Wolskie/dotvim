set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

"" Vundle plugins
""
Plugin 'gmarik/vundle'
Plugin 'scrooloose/nerdtree'
Plugin 'nanotech/jellybeans.vim'
Plugin 'tpope/vim-sensible'
Plugin 'Soares/butane.vim'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdcommenter'
Plugin 'myusuf3/numbers.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'skammer/vim-css-color'
Plugin 'sjl/gundo.vim'
Plugin 'bling/vim-airline'
Plugin 'vim-scripts/a.vim'
Plugin 'spf13/vim-autoclose'
Plugin 'Shougo/neocomplete.vim'
Plugin 'mbbill/undotree'

syntax on
set nobackup
set noswapfile

filetype plugin on
filetype indent on

set encoding=utf-8
set number

"" Set tabs to expand and
"" set there space
""
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

"" Hilight column and line
"" Where the curser is.
""
set cursorcolumn
set cursorline

"" Ignore casew when searching
""
set ignorecase
set smartcase
set showmatch
set hlsearch
set gdefault
set virtualedit=block
set hidden
set lazyredraw
set noshowmode 

set noswapfile
set ruler
set history=10000
set undofile
set undoreload=10000
set title
set autoindent
set autoread
set mouse=a

set foldmethod=syntax
set foldlevel=20

let mapleader=","
nnoremap <Leader>w :wa!<CR>
nnoremap <Leader>a :A!<CR>
nnoremap <Leader>q :bd!<CR>

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.

" Hilight column over 80 
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

nnoremap <F10> :set cursorline! cursorcolumn!<CR>  
nnoremap <F2> <ESC>:NERDTreeToggle<RETURN>

noremap <F4> :CtrlP .<CR>

" Pressing Tab or Shift-Tab will let you indent/unident selected lines
vnoremap <S-Tab> <gv
vnoremap <Tab> >gv

" No annoying sound on errors
"
set noerrorbells
set novisualbell
set t_vb=
set tm=500

"  move buffer with ALT arrow
nnoremap <A-left> :bprevious<cr>
nnoremap <A-right> :bnext<cr>

" Move buffer with CTRL TAB 
nnoremap <C-Tab> <ESC>:bnext<cr>
nnoremap <S-Tab> <ESC>:bprevious<cr>

" Neocomplete
let g:acp_enableAtStartup = 0
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1

" Let syntastic use clang
"
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

if has('gui_running')

    map <S-Insert> <MiddleMouse>
    map! <S-Insert> <MiddleMouse>

    let g:airline#extensions#tabline#enabled = 1
    let g:airline_powerline_fonts = 1

    set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 10

    set guicursor+=a:blinkon0       " Cursor doesn't blink - it's annoying
    set guioptions-=m               " No Menubar
    set guioptions-=T               " No Toolbar
    set guioptions-=l               " No Scrollbar left
    set guioptions-=L               " No Scrollbar left when split
    set guioptions-=r               " No Scrollbar right
    set guioptions-=r               " No Scrollbar right when split
    set laststatus=2                " always show statusline

    let g:airline_theme = 'jellybeans'
    colorscheme spacecadet

endif
